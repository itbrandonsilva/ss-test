import { StateServer } from './../src/server';
import { reducer } from './reducer'
import { createStore, applyMiddleware } from 'redux';

class Game {
    private store;
    private server: StateServer;
    private playerInputs = {};

    constructor(reducer) {
        this.server = new StateServer(() => this.store.getState(), this.receivePlayerInput.bind(this));
        this.store = createStore(reducer, applyMiddleware(this.server.reduxMiddleware.bind(this.server)));
        this.start();
    }

    start(interval=33) {
        setInterval(this.tick.bind(this), interval);
    }

    tick() {
        let events = this.server.getEvents();
        events.forEach(event => {
            switch (event.type) {
                case 'CONNECT':
                    console.log('CONNECTION');
                    return this.store.dispatch({type: 'ADD_PLAYER', playerId: event.clientId});
                case 'DISCONNECT':
                    console.log('DISCONNECTION');
                    return this.store.dispatch({type: 'REMOVE_PLAYER', playerId: event.clientId});
            }
        });

        for (var playerId in this.playerInputs) this.processInputs(playerId, this.playerInputs[playerId]);

        this.server.flush();
    }

    receivePlayerInput(input) {
        console.log(input);
        let playerInput = this.playerInputs[input.clientId] = this.playerInputs[input.clientId] || {};
        playerInput[input.name] = input.value;
    }

    processInputs(playerId, inputs) {
        if (inputs.left) return this.store.dispatch({type: 'MOVE_LEFT', playerId})
        if (inputs.right) return this.store.dispatch({type: 'MOVE_RIGHT', playerId})
    }
}

const game = new Game(reducer);