import * as Redux from 'redux';
import * as sio from 'socket.io-client/dist/socket.io.min.js';

export class StateClient {
    private clientId;
    private socket;
    private subscription;

    constructor(actionCb, host='127.0.0.1', port=7541) {
        this.socket = sio(host + ':' + port);

        this.socket.on('init', data => {
            this.clientId = data.clientId;
            actionCb([{type: 'INIT_STATE', clientId: data.clientId, state: data.state}]);
        });

        this.socket.on('actions', data => {
            actionCb(data.actions);
        });
    }

    sendInput(name, value) {
        if ( ! this.socket ) throw new Error('sendInput(): Must be connected to server before inputs can be sent.')
        this.socket.emit('input', {name, value});
    }
}