import * as sio from 'socket.io';

export class StateServer {
    private actions = [];
    private clients = {};
    private idPool = 0;
    private events = [];

    constructor(private getState, private inputHandler, port=7541) {
        let io = sio(port);

        io.on('connection', (socket) => {
            let clientId = (++this.idPool).toString();
            this.clients[clientId] = socket;
            socket.emit('init', { state: this.getState(), clientId });
            this.events.push({type: 'CONNECT', clientId});

            socket.on('input', (input) => {
                this.inputHandler({clientId, name: input.name, value: input.value});
            });

            socket.on('disconnect', () => {
                delete this.clients[clientId];
                this.events.push({type: 'DISCONNECT', clientId});
            });
        });
    }

    getEvents() {
        let copy = this.events.slice();
        this.events.length = 0;
        return copy;
    }

    reduxMiddleware({ getState }) {
        let push = this.push.bind(this);
        return (next) => {
            return (action) => {
                push(action);
                next(action);
            }
        }
    }

    push(action) {
        this.actions.push(action);
    }

    flush() {
        Object.keys(this.clients).forEach(clientId => {
            let client = this.clients[clientId];
            if (this.actions.length) client.emit('actions', {actions: this.actions});
        });
        this.actions.length = 0;
    }
}